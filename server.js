const express = require('express')
const app = express()
const port = 3000
const morgan = require('morgan')
const indexRouter = require('./router/user')

app.use(express.json())
app.use(express.urlencoded({extended: false}))
app.use(morgan('dev'));

app.use('/api', indexRouter)

app.listen(port, () => {
    console.log(`server running on port ${port}`)
})