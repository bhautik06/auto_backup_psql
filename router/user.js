const express = require('express')
const router = express.Router()
const db = require('../models')
const User = db.User

router.get('/', (req, res) => {
    User
        .findAll()
        .then(u => {
            res.send({u})
        })
        .catch(err => res.send(err))
})

router.post('/', (req, res) => {
    // Product.sync({ force: true }).then(() => {
    User
        .create({
            name: req.body.name,
            email: req.body.email,
            city: req.body.city
        })
        .then((u) => {
            const user = {
                name: req.body.name,
                email: req.body.email,
                city: req.body.city
            }
            if (u) {
                return res.status(200).send({ message: 'User Added...', Details: user });
            } else {
                res.status(400).send('Product Failed')
            }
        })
        .catch(err => {
            res.send(err)
        })
    // })
})

module.exports = router