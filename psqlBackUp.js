const cron = require('node-cron')
const moment = require('moment')
const exec  = require('child_process').exec

cron.schedule('0 * * * *', () => {
    const dbOptions = {
        host: 'localhost',
        user: 'postgres',
        password: '123',           
        port: '5432',
        database: 'product_app'
    };
    
    let dateTime = moment().format('YYYY-MM-DD-HH-mm-DD')            
    let filename = `db_backUp/${dbOptions.database}_${dateTime}.sql`
    let cmd = `pg_dump -h ${dbOptions.host} -U ${dbOptions.user} -p ${dbOptions.port} ${dbOptions.database} > ${filename}`
    // pg_dump -h localhost -U postgres -p 5432 LIS (Greater Than Sign) LIS_Latest.dump
    console.log(`${dbOptions.database} - Backup Running...`);
    exec(cmd, (err, stdout, stderr) => {
        if (err) {
            console.error(`exec error: ${err}`);
            return;
        }
        console.log('success!!');
    })
});